<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller{

	public function __construct(){
		parent::__construct();
		if ((!$this->session->has_userdata('usuario')) || ($this->session->userdata('usuario')->tipo_usuario == 1)){
			redirect(base_url());
		}
		$this->load->helper('frontend_helper'); 
		$this->load->model('Usuario_model', 'model');
		$this->load->model('Imagem_model','modelimagem');


	}



	public function cadastrar($dados = null){
		$dados['breadcrumbs'] = breadcrumbs($this->uri->segment(1), $this->uri->segment(2));

		$this->load->view('html_header');
		$this->load->view('header');
		$this->load->view('cadastro_Usuario',$dados);
		$this->load->view('footer');
		$this->load->view('html_footer');
	}

	public function index(){
		redirect('Usuario/visualizar');
	}

	public function visualizar($attribute = 'nome', $order_by = 'ASC', $quantidade = 10, $nome = '0', $inicio = 0){
		$this->load->library('pagination');
		$filtros = array();
		if(count($this->input->post()) != 0){ //se houve form.submit()
			$filtros['attribute'] = $this->input->post('attribute');
			$filtros['order_by'] = $this->input->post('order_by');
			$filtros['quantidade'] = $this->input->post('quantidade');
			$filtros['nome'] = $this->input->post('nome');

			if($filtros['attribute'] == '') $attribute = 'nome';
			else $attribute = $filtros['attribute'];

			if($filtros['order_by'] == '') $order_by = 'ASC';
			else$order_by = $filtros['order_by'];

			if($filtros['quantidade'] == '') $quantidade = 10;
			else $quantidade = $filtros['quantidade'];


			if($filtros['nome'] == '') $nome = '0';
			else $nome = $filtros['nome'];
		}
		else{
			$filtros['attribute'] = $attribute;
			$filtros['order_by'] = $order_by;
			$filtros['quantidade'] = $quantidade;
			if($nome != '0') $filtros['nome'] = $nome;
		}

		$num_rows = $this->model->num_rows($filtros);

		$config['base_url'] = base_url('Video/visualizar/'.$attribute.'/'.$order_by.'/'.$quantidade.'/'.$nome.'/');
		$config['total_rows'] = $num_rows; 
		$config['per_page'] = $filtros['quantidade']; 
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] = "</ul>";
		$config['first_link'] = FALSE; 
		$config['last_link'] = FALSE; 
		$config['first_tag_open'] = "<li>";
		$config['first_tar_close'] = "</li>";
		$config['prev_link'] = "Anterior";
		$config['prev_tag_open'] = "<li class='prev'>";
		$config['prev_tag_close'] = "</li>";
		$config['next_link'] = "Proximo";
		$config['next_tag_open'] = "<li class='next'>";
		$config['next_tag_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a href='#'>";
		$config['cur_tag_close'] = "</a></li>";
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['num_links'] = 3; 

		$qtde = $config['per_page'];
		//($this->uri->segment(3) != '') ? $inicio = $this->uri->segment(3) : $inicio = 0; 

		$this->pagination->initialize($config);

		
		//$dados['noticias'] = $this->modelnoticias->listar_noticias_paginadas($qtde, $inicio)->result(); 


		if(isset($filtros['attribute'])){
			if($filtros['attribute'] == 'nome') $filtros_verbose['attribute'] = 'Nome';
			else if($filtros['attribute'] == 'data') $filtros_verbose['attribute'] = 'Data de Registro';

		}
		if(isset($filtros['order_by'])){
			if($filtros['order_by'] == 'ASC') $filtros_verbose['order_by'] = 'Do menor para o maior';
			else if($filtros['order_by'] == 'DESC') $filtros_verbose['order_by'] = 'Do maior para o menor';
		}



		$dados['usuarios'] = $this->model->listar_usuarios_paginados_com_filtro($filtros, $inicio); 
		$dados['paginacao'] = $this->pagination->create_links();
		$dados['filtros'] = $filtros;
		$dados['filtros_verbose'] = $filtros_verbose;
		$dados['quantidade'] = $num_rows;
		
		
		$dados['breadcrumbs'] = breadcrumbs($this->uri->segment(1), $this->uri->segment(2));


		$this->load->view('html_header');
		$this->load->view('header');
		$this->load->view('ver_Usuarios',$dados);
		$this->load->view('footer');
		$this->load->view('html_footer');
	}

	public function confirmar_cadastro (){
		$this->load->model('Imagem_model','modelimagem');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('nome','nome','required');

		$dados['nome'] = $this->input->post('nome');
		$dados['email'] = $this->input->post('email');

		$senha = $this->input->post('senha');
		$confirmacao = $this->input->post('confirmacao');
		$dados['senha'] = $this->verificaSenha($senha,$confirmacao);


		$config['upload_path'] = FCPATH.'uploads';
		$config['allowed_types'] = 'jpg|jpeg|gif|png';
		$config['encrypt_name'] = TRUE;


		
		$this->load->library("upload",$config);

		$this->db->where('email', $dados['email']);
		$usuario = $this->db->get('usuario')->row();

		if($usuario == null){
			if($_FILES['nome_arquivo']['size'] != 0){

			//if($this->upload->do_upload('nome_arquivo')){

				$info_arquivo = $this->upload->data();
				$data['nome'] = $info_arquivo['file_name'];

				$nomedest = $config['upload_path']."/".$this->upload->file_name; 



				if($this->db->insert('imagem',$data)){	
					$imagem = $this->modelimagem->get_imagem($data['nome']);
					$dados['imagem_id'] = $imagem['id'];
				}
			}else{
				$dados['imagem_id'] = 1;
			}



			if($dados['senha'] == null){
				$this->session->set_flashdata('erro', 'Confirmação de senha não bate com a senha digitada.');
				redirect('Usuario/visualizar');
			}

			if($this->form_validation->run() == FALSE){
				$this->cadastrar();
			}else{ 
				if ($this->db->insert('usuario',$dados) == FALSE){
					$this->session->set_flashdata('cadastrado', 'Usuário cadastrado com sucesso!');
				}else{
					$this->session->set_flashdata('erroCadastro');
				}
			}
		}
		else{
			$this->session->set_flashdata('erro', 'Ocorreu um erro no cadastro, pois e-mail já está em uso.');			
		}
		redirect('Usuario/visualizar');
		//$this->cadastrar();
	}

	public function verificaSenha($password,$confirmacao){
		if ($password == $confirmacao){ 	
			$options = ['cost' => 12];
	        //$password = 'A/15H#%ER%W#$%W'.$password.'%51%444SDeS'; //salt 1 e 2
			$password = password_hash($password, PASSWORD_DEFAULT, $options);
        	//$senhaCrypt = md5($senha);
			$senhaCrypt = $password;
			return $senhaCrypt; 
		}else{
			return(null);
		}
	}

	public function update(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome','nome','required');
		$this->form_validation->set_rules('email','email','required');

		//$id = $this->input->post('id');
		$email = $this->input->post('email');
		$dados['nome'] = $this->input->post('nome');
		//$dados['email'] = $this->input->post('email');
		$dados['tipo_usuario'] = $this->input->post('tipo');

		$config['upload_path'] = FCPATH.'uploads';
		$config['allowed_types'] = 'jpg|jpeg|gif|png';
		$config['encrypt_name'] = TRUE;
		$this->load->library("upload",$config);

		if($_FILES['arquivo']['size'] != 0){

			if($this->upload->do_upload('arquivo')){

				$info_arquivo = $this->upload->data();
				$data['nome'] = $info_arquivo['file_name'];

				if($this->db->insert('imagem',$data)){	
					$imagem = $this->modelimagem->get_imagem($data['nome']);
					$dados['imagem_id'] = $imagem['id'];
				}
			}else{
				$this->session->set_flashdata('erro','Ocorreu um erro no carregamento da imagem.');
				redirect(base_url('Usuario/visualizar'));
			}
		}
		

		if ($this->form_validation->run() == FALSE){
			
			$this->session->set_flashdata('erro', 'Ocorreu um erro na validação dos dados.');
		}
		else{
			if($this->input->post('senha') != ''){
				$senha_crypt = $this->verificaSenha($this->input->post('senha'), $this->input->post('repetir-senha'));
				if($senha_crypt != null){
					$dados['senha'] = $senha_crypt;
					
				}else{
					$this->session->set_flashdata('erro', 'Confirmação de senha não bate com a senha digitada.');
					redirect(base_url('Usuario/visualizar'));

				}
			}
			if ($this->model->update($email,$dados)){


				$this->session->set_flashdata('editado',TRUE);
			}
			else{
				$this->session->set_flashdata('erro', 'Ocorreu um erro ao atualizar dos dados.');
			}

			
			redirect(base_url('Usuario/visualizar'));
		}
	} 

	public function delete(){
		$email = $this->input->post('email');
		$this->session->set_flashdata('deletado',TRUE);
		$this->db->where('email',$email);
		$this->db->delete('usuario');
		redirect(base_url('Usuario/visualizar'));
	}


}