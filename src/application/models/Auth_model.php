<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getPasswordHash($email){
		$this->db->where('email', $email);
   		$usuario = $this->db->get('usuario')->row();
   		return ($usuario!= NULL) ? $usuario->senha : false;
	}

}